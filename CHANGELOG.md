# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keepachangelog], and this project adheres to [Semantic Versioning][semver].

## [Unreleased]

## [2.0.1] - 2021-04-05
### Fixed
* Accept multiple response codes for webapp creation and file upload

## [2.0.0] - 2021-04-05
### Added
* Document code contribution guidelines
* Add support for Python 3.9
* Format code and sort imports on tox run
* Provide constants used by PythonAnywhere
* Implement basic actions for consoles

### Changed
* Encapsulate concrete request to minimise redundant code
* Adapt changed response code after successful webapp creation
* Unify tests and implementation details
* Restructure development extras

### Fixed
* Correct environment variable names on lookup

### Removed
* Removed support for Python 3.6

## [1.1.0] - 2020-03-01
### Added
* Provide quota and response errors
* Implement function to construct valid payloads
* Provide mocked responses for tests
* Implement basic actions for schedules

### Changed
* Pull client configuration out of global scope
* Improve and unify webapp along the lines of schedule
* Output more verbose message in ResponseError

### Removed
* Clean commented code snippets

### Fixed
* Ensure base url is lowercase
* Expect correct http response codes

## [1.0.1] - 2020-02-27
### Changed
* Extend webapp usage examples

### Fixed
* Allow falsy values when modifying a webapp
* Unify mixed up package and project names as well as links

## [1.0.0] - 2020-02-27
### Added
* Implement getenv function that ensures namespaced environment variables
* Define global configuration
* Provide base client responsible for authentication and requests
* Implement basic actions for webapps
* Implement basic actions for files

[keepachangelog]: https://keepachangelog.com/en/1.0.0/
[semver]: https://semver.org/spec/v2.0.0.html
