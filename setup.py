from setuptools import find_namespace_packages, setup

# Installation dependencies
install_requires = [
    'requests>=2.23,<3.0',
]

# Linting dependencies
linting_extras = [
    'brunette>=0.2.0,<1.0',
    'flake8>=3.7,<4.0',
    'isort>=5.7,<6.0',
]

# Testing dependencies
testing_extras = [
    'coverage>=5.0,<6.0',
    'pytest>=5.3,<6.0',
    'responses>=0.10,<1.0',
    'tox>=3.14,<4.0',
]

setup(
    packages=find_namespace_packages(exclude=['tests']),
    include_package_data=True,
    license='MIT License (MIT)',
    description='Thin wrapper for the PythonAnywhere API.',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/texperience/pythonanywhereapiclient',
    author='Timo Rieber',
    author_email='trieber@texperience.de',
    install_requires=install_requires,
    extras_require={
        'linting': linting_extras,
        'testing': testing_extras,
    },
    classifiers=[
        # See https://pypi.org/classifiers/
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Software Development',
    ],
)
