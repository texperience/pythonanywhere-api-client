from pythonanywhereapiclient.client import Client


def test_base_url_is_valid_using_environment_variables(data):
    host, token, user = (
        value for key, value in data.items() if key in ['host', 'token', 'user']
    )
    client = Client()

    assert f'https://{host}/api/v0/user/{user}/'.lower() == client.base_url


def test_base_url_is_valid_using_parameters(data):
    host, token, user = (
        f'param_{value}'
        for key, value in data.items()
        if key in ['host', 'token', 'user']
    )
    client = Client(host=host, token=token, user=user)

    assert f'https://{host}/api/v0/user/{user}/'.lower() == client.base_url


def test_endpoint_url_is_valid(data):
    host, token, user = (
        value for key, value in data.items() if key in ['host', 'token', 'user']
    )
    endpoint = 'testendpoint/'
    endpoint_url = Client()._construct_endpoint_url(endpoint)

    assert f'https://{host}/api/v0/user/{user}/{endpoint}' == endpoint_url


def test_request_headers_contain_auth_token(data):
    host, token, user = (
        value for key, value in data.items() if key in ['host', 'token', 'user']
    )
    headers = Client()._headers()

    assert headers['Authorization'] == f'Token {token}'
